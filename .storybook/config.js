import { configure, addDecorator } from '@storybook/react';
import { withA11y } from '@storybook/addon-a11y';

import '../src/index.css';

// Add the accessibility decorator to all stories
addDecorator(withA11y);

configure(require.context('../src/', true, /\.stories\.(js|mdx)$/), module);

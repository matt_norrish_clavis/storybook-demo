import React from 'react';
import PropTypes from 'prop-types';
import { Provider as ReduxProvider } from 'react-redux';
import { createStore } from 'redux';
import { devToolsEnhancer } from 'redux-devtools-extension';
import reducer from '../../src/reducer';
import { INITIAL_STATE } from '../../src/store';

const configureStore = (initialState = INITIAL_STATE, name) => createStore(
    reducer,
    initialState,
    devToolsEnhancer({ name: `Storybook: ${name}` }),
);

// This is a Storybook 'decorator'
// A decorator allows you to wrap stories with other components.
// This is helpful when an application uses context providers.
const decorator = (story, options) => (
    <ReduxProvider store={configureStore(options.parameters.initialReduxState, options.story)}>
        {story()}
    </ReduxProvider>
);

export default decorator;
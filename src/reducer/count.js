const INITIAL_STATE = 0

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case 'COUNT_INCREMENT': return state + 1;
        case 'COUNT_DECREMENT': return state - 1;
        case 'COUNT_RESET': return INITIAL_STATE;
        default: return state;
    }
}

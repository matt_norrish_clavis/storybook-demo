import { createStore } from 'redux';
import { devToolsEnhancer } from 'redux-devtools-extension';
import reducer from './reducer';

export const INITIAL_STATE = {
    count: 0,
};

export default createStore(reducer, INITIAL_STATE, devToolsEnhancer());

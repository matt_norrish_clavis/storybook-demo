import React from 'react';
import logo from './logo.svg';
import classes from './index.module.css';

const Logo = () => <img src={logo} className={classes.logo} alt="logo" />;

export default Logo;

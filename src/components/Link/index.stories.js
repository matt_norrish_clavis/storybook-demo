import React from 'react';
import Link from '.';

export default {
    title: 'Link',
    component: Link,
};

export const sample = () => {
    return <Link href="#">Sample text</Link>;
};

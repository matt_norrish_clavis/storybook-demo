import React from 'react';
import PropTypes from 'prop-types';
import classes from './index.module.css';

const Link = ({ href, children, ...otherProps }) => (
    <a
        className={classes.link}
        href={href}
        {...otherProps}
    >
        {children}
    </a>
);

Link.propTypes = {
    href: PropTypes.string.isRequired,
    children: PropTypes.node,
};
Link.defaultProps = {
    children: null,
};

export default Link;

import React from 'react';
import { withKnobs, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';
import Button from './Button'

export default {
    title: 'Counter/Button',
    component: Button,
    decorators: [withKnobs],
};

export const sample = () => {
    // An event handler which will be logged in the Actions panel
    const onClick = action('button-click');

    // An value which will be editable in the Knobs panel
    const children = text('children', 'Sample text');

    return <Button onClick={onClick}>{children}</Button>;
};

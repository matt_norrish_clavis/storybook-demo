import React from 'react';
import { withKnobs, boolean } from '@storybook/addon-knobs';
import reduxDecorator from '../../../.storybook/decorators/redux';
import Counter from '.';

export default {
    title: 'Counter',
    component: Counter,
    decorators: [reduxDecorator, withKnobs],
    parameters: {
        componentSubtitle: 'A component which uses the count value from Redux state and dispatches actions to manipulate it.',
    },
};

export const defaultProps = () => <Counter />;

export const showReset = () => {
    const showReset = boolean('showReset', true);
    return <Counter showReset={showReset} />;
};

export const withRenderProp =  () => {
    const showReset = boolean('showReset', false);
    return (
        <Counter showReset={showReset}>
            {count => <>The count is {count}</>}
        </Counter>
    );
};

export const with50 = () => {
    const showReset = boolean('showReset', false);
    return <Counter showReset={showReset} />;
};
with50.story = {
    name: 'With initial count of 50',
    parameters: {
        initialReduxState: { count: 50 }, // initial Redux state defined
    },
};

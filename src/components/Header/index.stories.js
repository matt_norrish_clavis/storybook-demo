import React from 'react';
import Header from '.';

export default {
    title: 'Header',
    component: Header,
};

export const sample = () => (
    <Header>
        <p>Hello, World!</p>
        <p>I'm in the Header!</p>
    </Header>
);

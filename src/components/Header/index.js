import React from 'react';
import PropTypes from 'prop-types';
import classes from './index.module.css';

const Header = ({ children }) => (
    <header className={classes.header}>
        {children}
    </header>
);

Header.propTypes = {
    children: PropTypes.node,
};

Header.defaultProps = {
    children: null,
};

export default Header;
